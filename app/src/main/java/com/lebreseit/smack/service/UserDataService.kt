package com.lebreseit.smack.service

import android.graphics.Color
import com.lebreseit.smack.controller.App
import java.util.*

/**
 * Created by Chris on 21/02/2018.
 */
object UserDataService {

    var id =""
    var avatarColour = ""
    var avatarName = ""
    var email= ""
    var name = ""

    fun logout(){
        id=""
        avatarColour=""
        avatarName=""
        email=""
        name=""
        App.prefs.authToken=""
        App.prefs.userEmail=""
        App.prefs.isLoggedIn=false
        MessageService.clearMessages()
        MessageService.clearChannels()
    }

fun returnAvatarColour(components: String): Int{
    //[r, g, b, a]
    val strippedColour = components
            .replace("[","")
            .replace("]","")
            .replace(",","")
    var r = 0
    var g = 0
    var b = 0

    val scanner = Scanner(strippedColour)
    if(scanner.hasNext()){
        r = (scanner.nextDouble()*255).toInt()
        g = (scanner.nextDouble()*255).toInt()
        b = (scanner.nextDouble()*255).toInt()
    }

    return Color.rgb(r,g,b)
}

}