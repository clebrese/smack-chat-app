package com.lebreseit.smack.service

import android.content.Context
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.lebreseit.smack.controller.App
import com.lebreseit.smack.model.Channel
import com.lebreseit.smack.model.Message
import com.lebreseit.smack.utility.URL_GET_CHANNELS
import com.lebreseit.smack.utility.URL_GET_MESSAGES
import org.json.JSONException

/**
 * Created by Chris on 25/02/2018.
 */
object MessageService {

    val channels = ArrayList<Channel>()
    val messages = ArrayList<Message>()

    fun getChannels(complete: (Boolean) -> Unit ){

        clearChannels()

        val channelsRequest =object : JsonArrayRequest(Method.GET, URL_GET_CHANNELS, null,
                Response.Listener {response ->
                    try {
                        for (x in 0 until response.length()){
                            val channel = response.getJSONObject(x)
                            val channelName = channel.getString("name")
                            val channelDesc = channel.getString("description")
                            val channelId = channel.getString("_id")
                            val newChannel = Channel(channelName,channelDesc,channelId)
                            MessageService.channels.add(newChannel)
                        }
                        complete(true)

                    } catch (e: JSONException) {
                    Log.d("JSON", "EXC:"+e.localizedMessage)
                        complete(false)
                }
                }, Response.ErrorListener {error ->
                    Log.d("ERROR","Couldn't retrieve channels")
                    complete(false)

        }){
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String,String>()
                headers.put("Authorization","Bearer ${App.prefs.authToken}")
                return headers
            }
        }

        App.prefs.requestQueue.add(channelsRequest)
    }

    fun getMessagess( channelId: String,complete: (Boolean) -> Unit ){
        clearMessages()

        val url = "$URL_GET_MESSAGES$channelId"
        val messagesRequest =object : JsonArrayRequest(Method.GET, url, null,
                Response.Listener {response ->
                    try {
                        for (x in 0 until response.length()){
                            val message = response.getJSONObject(x)
                            val id = message.getString("_id")
                            val messageBody = message.getString("messageBody")
                            val msgChannelId = message.getString("channelId")
                            val userName = message.getString("userName")
                            val userAvatar = message.getString("userAvatar")
                            val userAvatarColor = message.getString("userAvatarColor")
                            val timeStamp = message.getString("timeStamp")

                            val newMessage = Message(messageBody,userName,msgChannelId,userAvatar,
                                    userAvatarColor,id,timeStamp)
                            MessageService.messages.add(newMessage)
                        }
                        complete(true)

                    } catch (e: JSONException) {
                        Log.d("JSON", "EXC:"+e.localizedMessage)
                        complete(false)
                    }
                }, Response.ErrorListener {error ->
            Log.d("ERROR","Couldn't retrieve channels")
            complete(false)

        }){
            override fun getBodyContentType(): String {
                return "application/json; charset=utf-8"
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String,String>()
                headers.put("Authorization","Bearer ${App.prefs.authToken}")
                return headers
            }
        }

        App.prefs.requestQueue.add(messagesRequest)
    }

    fun clearMessages(){
        messages.clear()
    }

    fun clearChannels(){
        channels.clear()
    }
}