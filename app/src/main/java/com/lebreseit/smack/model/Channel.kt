package com.lebreseit.smack.model


/**
 * Created by Chris on 25/02/2018.
 */
class Channel(val name: String, val description: String, val id: String) {

    override fun toString(): String {
        return "# $name";
    }
}