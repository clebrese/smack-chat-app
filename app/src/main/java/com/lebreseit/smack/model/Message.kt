package com.lebreseit.smack.model

/**
 * Created by Chris on 28/02/2018.
 */
class Message constructor(val message: String, val userName: String,
                          val channelId: String, val userAvatar: String,
                          val userAvatarColour: String, val id: String,
                          val timeStamp: String) {
}