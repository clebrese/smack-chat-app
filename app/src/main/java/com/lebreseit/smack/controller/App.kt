package com.lebreseit.smack.controller;

import android.app.Application
import com.lebreseit.smack.utility.SharedPrefs

/**
 * Created by Chris on 26/02/2018.
 */

class App : Application() {

    companion object {
        lateinit var prefs : SharedPrefs
    }

    override fun onCreate() {
        prefs = SharedPrefs(applicationContext)
        super.onCreate()
    }
}
