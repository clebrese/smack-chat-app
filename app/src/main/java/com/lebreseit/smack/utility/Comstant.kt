package com.lebreseit.smack.utility

/**
 * Created by Chris on 20/02/2018.
 */

const val BASE_URL = "http://192.168.1.111:3005/v1/"
const val SOCKET_URL = "http://192.168.1.111:3005/"

//const val BASE_URL = "https://devslopes-chattin.herokuapp.com/v1/"
const val URL_REGISTER = "${BASE_URL}account/register"
const val URL_LOGIN = "${BASE_URL}account/login"
const val URL_CREATE_USER = "${BASE_URL}user/add"
const val URL_GET_USER="${BASE_URL}user/byEmail/"
const val URL_GET_CHANNELS="${BASE_URL}channel"
const val URL_GET_MESSAGES="${BASE_URL}message/byChannel/"

//Broadcast Constants
const val BCAST_USER_DATA_CHANGE = "BCAST_USER_DATA_CHANGED"
